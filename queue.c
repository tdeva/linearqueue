#include <stdio.h>

#define BUFFER_SIZE 10
int buffer[BUFFER_SIZE];
int front = 0, rear = 0;

void enqueue(int data) {
	if (rear < BUFFER_SIZE) {
		printf("Add %d to buffer\n", data);
		buffer[rear++] = data;
	} else {
		printf("Buffer overflow\n");
	}
}

int dequeue() {
	if (rear > 0 && front < rear) {
		int data = buffer[front++];
		printf("Consumed %d from buffer\n", data);
	} else {
		printf("Buffer underflow\n");
		return 1;
	}
}

void print_buffer() {
        if (rear > 0 && front < rear) {
		printf("Buffer contents\n");
		for (int i = front; i < rear; i++)
                	printf("[%d]: %d -> ", i, buffer[i]);
		printf("\n");
        } else {
                printf("Buffer empty\n");
        }
}


int main()
{
	//Add 5 elements
	for (int i = 1; i <=5; i++)
		enqueue(i);
	print_buffer();
	//Remove 3 elements
	for (int i = 1; i <= 3; i++)
		dequeue(i);
	print_buffer();
	//Add 10 elements
	for (int i = 1; i <=10; i++)
		enqueue(i);
	print_buffer();
	//Remove 20 elements
	for (int i = 1; i <= 20; i++)
		dequeue(i);
	print_buffer();
}
